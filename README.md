# titro

A python implementation of the card game known as Titro, a 2-player imperfect information zero-sum game played with tarot cards. See a multiplayer web implementation at [titro.8oc.org](https://titro.8oc.org).
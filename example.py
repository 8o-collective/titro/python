import titro

NUM_SIMULATIONS = 10000

PLAYER_ONE = lambda game: titro.bots.SimpleBot(game, titro.Orientation.FORWARD)
PLAYER_TWO = lambda game: titro.bots.SimpleBot(game, titro.Orientation.BACKWARD)

def simulate_game():
	game = titro.Titro()
	game.players = (PLAYER_ONE(game), PLAYER_TWO(game))
	
	# this is the basic loop all titro engines must adhere to
	while not game.game_over:
		game.prep()
		for phase in (titro.Phase.PRIMA, titro.Phase.RESPAIDA):
			for player in game.players:
				player.attack(*player.move())
			game.phase = phase
		game.cleanup()
	
	return game.winner if not game.winner else game.players.index(game.winner)

if __name__ == '__main__':
	player_wins = {
		0: 0,
		1: 0,
		None: 0 # i know this is abysmal mkay
	}

	for i in range(NUM_SIMULATIONS):
		player_wins[simulate_game()] += 1
	
	winner = max(player_wins, key=player_wins.get)

	try:
		win_ratio = player_wins[winner] // player_wins[not winner]
	except ZeroDivisionError:
		win_ratio = '∞'

	print(
		f"""
		Player 0 ({PLAYER_ONE(None).__class__.__name__}) won: {player_wins[0]} games
		Player 1 ({PLAYER_TWO(None).__class__.__name__}) won: {player_wins[1]} games
		Ties: {player_wins[None]}

		Win ratio is approximately {win_ratio if 0 == winner else 1}:{win_ratio if 1 == winner else 1}

		Player {winner} won {(player_wins[winner] / 10000) * 100}% of games.
		"""
	)
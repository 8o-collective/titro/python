from .card import Card
from .constants import SCORE_LIMIT, ROUND_LIMIT, MajorArcana, Orientation, Phase, Positions
from .exceptions import IllegalError
from .player import Player
from .table import Table

class Titro:
    def __init__(self):
        self.round = 0
        self.phase = Phase.CLEANUP 
        self.attacks = {position: {} for position in Positions.Absolute}
        self.game_over = False

        self.table = Table()
        self.players = (Player(self, orientation=Orientation.FORWARD), 
                        Player(self, orientation=Orientation.BACKWARD))
    
    def __repr__(self):
        return f"Phase: {self.phase.name}\nTable: {self.table}\nPlayers: {self.players}"

    def prep(self):
        if self.phase != Phase.CLEANUP:
            raise IllegalError('Cannot prep before cleanup.')
        self.phase = Phase.PREP
        self.table.spread.draw()
        for player in self.players:
            player.draw()

    def calculate_score(self):
        scores = {player: 0 for player in self.players}

        for position in Positions.Absolute:
            spread_card = self.table.spread.cards[position]

            score_func = lambda card: abs(card.rank.value - spread_card.rank.value)

            # no attackers, hence no points awarded
            if not self.attacks[position]:
                continue

            attackers, cards = zip(*self.attacks[position].items())

            get_attack = lambda card_func: [{'attacker': attacker, 'card': card} for attacker, card in zip(attackers, cards) if card_func(card)][0] # get { player: card } from card condition

            # one attacker wins unless fool
            if len(attackers) == 1 and cards[0].rank is not MajorArcana.FOOL:
                scores[attackers[0]] += score_func(cards[0])

            # two attackers... pray.
            if len(attackers) == 2:
                if any((card.rank is MajorArcana.FOOL for card in cards)): # if a player fools, other player wins
                    nonfool = get_attack(lambda card: card is not MajorArcana.FOOL)
                    scores[nonfool['attacker']] += score_func(nonfool['card'])
                
                if all(suitmatch := [card.suit == spread_card.suit for card in cards]) or not any(suitmatch): # if both or neither cards match suit, higher wins
                    if len(set((card.rank.value for card in cards))) != 1: # cards aren't the same value (continue if they are)
                        position_winning = get_attack(lambda card: card is max(cards))
                        scores[position_winning['attacker']] += score_func(position_winning['card'])
                else: # one of the cards matches suit
                    matching_suit = get_attack(lambda card: card.suit is spread_card.suit)
                    scores[matching_suit['attacker']] += score_func(matching_suit['card'])
        
        if 0 in scores.values(): # split
            scores = {player: max(scores.values()) for player in self.players}
        
        for player in self.players:
            player.score += scores[player]

    def cleanup(self):
        self.phase = Phase.CLEANUP

        self.calculate_score()

        # wipe board and hands
        nd_attack_cards = [self.attacks[position].values() for position in Positions.Absolute] # returns 2d array of attack cards
        attack_cards = [card for cardlist in nd_attack_cards for card in cardlist] # flattened
        self.table.discard.add(attack_cards)
        self.attacks = {position: {} for position in Positions.Absolute}

        for player in self.players:
            player.discard()
        self.table.spread.discard()

        self.round += 1

        # check if game over conditions met
        if any([player.score > SCORE_LIMIT for player in self.players]) or self.round == ROUND_LIMIT:
            self.end_game()

    def end_game(self):
        self.game_over = True

        if self.players[0].score > self.players[1].score:
            self.winner = self.players[0]
            self.loser = self.players[1]
        elif self.players[0].score < self.players[1].score:
            self.winner = self.players[1]
            self.loser = self.players[0]
        else:
            self.winner = None
            self.loser = None

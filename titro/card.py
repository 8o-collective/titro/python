from dataclasses import dataclass, field
from typing import Union

from .constants import Suit, Rank, MajorArcana

@dataclass(order=True)
class Card:
	rank: Union[Rank, MajorArcana] = field(compare=True) # value is simply self.rank.value
	suit: Suit = field(compare=True)
	# we used to track position on board here too, but it actually doesn't matter
	# it's redundant to keep the position of the card when you know where it is based on what list it's in

	def __post_init__(self):
		self.rank = Rank(self.rank) if self.suit != Suit.MAJORARCANA else MajorArcana(self.rank)
		self.suit = Suit(self.suit)

	def asdict(self):
		return { 'rank': self.rank.value, 'suit': self.suit.value }
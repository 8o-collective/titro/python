from enum import Enum, auto

HAND_SIZE = 5
SCORE_LIMIT = 66
ROUND_LIMIT = 6

class Phase(int, Enum):
    PREP        = 0
    PRIMA       = 1
    RESPAIDA    = 2
    CLEANUP     = 3

    def next(self):
        cls = self.__class__
        members = list(cls)
        index = members.index(self) + 1
        if index >= len(members):
            index = 0
        return members[index]

class Suit(int, Enum):
    MAJORARCANA = 0
    CUPS        = 1
    PENTACLES   = 2
    WANDS       = 3
    SWORDS      = 4

class Rank(int, Enum):
    ACE     = 1
    TWO     = 2
    THREE   = 3
    FOUR    = 4
    FIVE    = 5
    SIX     = 6
    SEVEN   = 7
    EIGHT   = 8
    NINE    = 9
    TEN     = 10
    PAGE    = 11
    KNIGHT  = 12
    QUEEN   = 13
    KING    = 14

class MajorArcana(int, Enum):
    FOOL            = 0
    MAGICIAN        = 1
    HIGHPRIESTESS   = 2
    EMPRESS         = 3
    EMPEROR         = 4
    HIEROPHANT      = 5
    LOVERS          = 6
    CHARIOT         = 7
    STRENGTH        = 8
    HERMIT          = 9
    WHEELOFFORTUNE  = 10
    JUSTICE         = 11
    HANGEDMAN       = 12
    DEATH           = 13
    TEMPERANCE      = 14
    DEVIL           = 15
    TOWER           = 16
    STAR            = 17
    MOON            = 18
    SUN             = 19
    JUDGEMENT       = 20
    WORLD           = 21

class Orientation(Enum):
    FORWARD     = auto() # cloak on right, blind on left
    BACKWARD    = auto() # cloak on left, blind on right

class Positions():
    class Absolute(int, Enum):
        LEFT        = 1
        CENTER      = 0
        RIGHT       = -1

        def to_relative(self, orientation: Orientation):
            cls = self.__class__
            members = list(cls)
            index = members.index(self)

            if orientation == Orientation.FORWARD:
                return list(Positions.Relative)[index]
            if orientation == Orientation.BACKWARD:
                return list(Positions.Relative)[2 - index]
    
    class Relative(int, Enum):
        BLIND       = -1
        CENTER      = 0
        CLOAK       = 1

        def to_absolute(self, orientation: Orientation):
            cls = self.__class__
            members = list(cls)
            index = members.index(self)

            if orientation == Orientation.FORWARD:
                return list(Positions.Absolute)[index]
            if orientation == Orientation.BACKWARD:
                return list(Positions.Absolute)[2 - index]
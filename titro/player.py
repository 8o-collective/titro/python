import uuid

from .constants import HAND_SIZE, Orientation, Phase, Positions
from .exceptions import IllegalError

class Player:
	def __init__(self, game, orientation: Orientation):
		self.game = game
		self.id = uuid.uuid4()
		self.score = 0
		self.hand = []
		self.attacking = None
		self.orientation = orientation
	
	def __repr__(self):
		return f"\n\tID: {self.id}\n\tScore: {self.score}\n\tHand: {self.hand}\n\tOrientation: {self.orientation}\n"
	
	def __hash__(self):
		return self.id.int

	def draw(self):
		self.hand = [self.game.table.deck.draw() for _ in range(HAND_SIZE)]

	def spread(self):
		return {
				Positions.Relative.BLIND: 	self.game.table.spread.cards[Positions.Relative.BLIND.to_absolute(self.orientation)],
				Positions.Relative.CENTER: 	self.game.table.spread.cards[Positions.Relative.CENTER.to_absolute(self.orientation)], 
				Positions.Relative.CLOAK: 	self.game.table.spread.cards[Positions.Relative.CLOAK.to_absolute(self.orientation)]
			} if self.game.phase in (Phase.CLEANUP, Phase.PRIMA) else {
				Positions.Relative.CENTER: 	self.game.table.spread.cards[Positions.Relative.CENTER.to_absolute(self.orientation)], 
				Positions.Relative.CLOAK: 	self.game.table.spread.cards[Positions.Relative.CLOAK.to_absolute(self.orientation)] 
			}

	def attack(self, card, position: Positions.Relative):
		if card not in self.hand:
			raise IllegalError('Cannot attack with card that is not in hand.')

		self.game.attacks[position.to_absolute(self.orientation)].update({self: card})
		self.hand.remove(card)

		self.attacking = {'position': position, 'card': card.asdict()}

	def discard(self):
		self.game.table.discard.add(self.hand)
		self.hand = []
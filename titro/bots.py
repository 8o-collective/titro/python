from abc import ABC, abstractmethod

from .card import Card
from .constants import Phase, Positions
from .player import Player

class TitroBot(Player, ABC):
    @abstractmethod
    def move(self):
        # Logic goes here. Defaults to attacking blind/center (on prima and respaida respectively) with the first card in hand.
        pass

class SimpleBot(TitroBot):
    def move(self):
        if self.game.phase == Phase.RESPAIDA:
            return self.hand[0], Positions.Relative.CENTER
        return self.hand[0], Positions.Relative.BLIND

class Maximumer(TitroBot):
    def move(self):
        spread = self.attackable_spread()
        if self.game.phase == 'prima':
            # if we're in prima we need to estimate the blind
            # for now we just estimate a majorarcana with a value of 9
            spread.update({self.blind: Card('hermit', 'majorarcana')})

        cardscorelist = [[abs(handcard.value - spread[position].value), position, handcard] for position in spread.keys() for handcard in self.hand]
        scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
        scoreindex = scorelist.index(max(scorelist))
        totalmove = cardscorelist[scoreindex]
        return totalmove[2], totalmove[1]

class DelusionalMaximumer(TitroBot):
    """
    a version of maximumer that doesn't estimate blind and doesn't attack on it.
    actually a bit better than maximumer in testing
    """
    def move(self):
        spread = self.attackable_spread()

        cardscorelist = [[abs(handcard.value - spread[position].value), position, handcard] for position in spread.keys() for handcard in self.hand]
        scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
        scoreindex = scorelist.index(max(scorelist))
        totalmove = cardscorelist[scoreindex]
        return totalmove[2], totalmove[1]

class Minimumer(TitroBot):
    """
    a version of maximumer that chooses the lowest value instead of the highest value.
    drops 94% of games to the random, default TitroBot class.
    """
    def move(self):
        spread = self.spread()
        if self.primapos:
            spread.pop(self.primapos)
        else:
            # if no primapos then we're in prima and hence we need to estimate the blind
            # for now we just estimate a majorarcana with a value of 9
            spread.update({self.blind: Card('hermit', 'majorarcana')})

        cardscorelist = [[abs(handcard.value - spread[position].value), position, handcard] for position in spread.keys() for handcard in self.hand]
        scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
        scoreindex = scorelist.index(min(scorelist))
        totalmove = cardscorelist[scoreindex]
        return totalmove[2], totalmove[1]

# class Splitter(TitroBot):
#     """
#     this fellow always plays the lowest value cards in the highest value positions,
#     and always attempts to lose to whatever the enemy played in the prima.
#     theoretically this makes it split the game as often as possible.
#     practically it's slightly worse than random.
#     """
#     def move(self):
#         spread = self.attackable_spread()

#         if self.game.phase == 'prima':
#             minvalcard = min(self.hand, key=lambda card: card.value)
#             differences = [[abs(minvalcard.value - spread[spreadpos].value), spreadpos, minvalcard] for spreadpos in spread.keys()]
#             scorelist = [cardscore[0] for cardscore in differences]
#             scoreindex = scorelist.index(max(scorelist))
#             totalmove = differences[scoreindex]
#             return totalmove[2], totalmove[1]

#         if self.game.phase == 'respaida':
#             # find where enemy attacked in prima
#             minvalcard = min(self.hand, key=lambda card: card.value)
#             attackerpos = self.game.get_player_attacks(PLAYER_IDS[0] if self.id == PLAYER_IDS[1] else PLAYER_IDS[1])[0]
#             if attackerpos == self.primapos:
#                 differences = [[abs(minvalcard.value - spread[spreadpos].value), spreadpos, minvalcard] for spreadpos in spread.keys()]
#                 scorelist = [cardscore[0] for cardscore in differences]
#                 scoreindex = scorelist.index(max(scorelist))
#                 totalmove = differences[scoreindex]
#                 return totalmove[2], totalmove[1]
#             else:
#                 return minvalcard, attackerpos

class Splitmax(TitroBot):
    """
    plays like splitter in prima, maximumer in respaida
    """
    def move(self):
        spread = self.attackable_spread()

        if self.game.phase == 'prima':
            minvalcard = min(self.hand, key=lambda card: card.value)
            differences = [[abs(minvalcard.value - spread[spreadpos].value), spreadpos, minvalcard] for spreadpos in spread.keys()]
            scorelist = [cardscore[0] for cardscore in differences]
            scoreindex = scorelist.index(max(scorelist))
            totalmove = differences[scoreindex]
            return totalmove[2], totalmove[1]

        if self.game.phase == 'respaida':
            cardscorelist = [[abs(handcard.value - spread[position].value), position, handcard] for position in spread.keys() for handcard in self.hand]
            scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
            scoreindex = scorelist.index(max(scorelist))
            totalmove = cardscorelist[scoreindex]
            return totalmove[2], totalmove[1]

class MaxSuit(TitroBot):
    """
    tries to play for maximum value in-suit
    """
    def move(self):
        maxvalcard = max(self.hand, key=lambda card: card.value)
        spread = self.attackable_spread()

        if self.game.phase == 'prima':
            centersuit = spread['center'].suit
            cloaksuit = spread[self.cloak].suit
            handsuits = [handcard.suit for handcard in self.hand]
            # copied this code directly from stackoverflow blame them if it doesn't work
            centerselection = None
            cloakselection = None
            if centersuit in handsuits:
                indices = [i for i, x in enumerate(handsuits) if x == centersuit]
                suitmatches = [self.hand[index] for index in indices]
                cardscorelist = [[abs(handcard.value - spread['center'].value), handcard] for handcard in suitmatches]
                scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
                scoreindex = scorelist.index(max(scorelist))
                # centerselection = [value, card]
                centerselection = cardscorelist[scoreindex]

            if cloaksuit in handsuits:
                indices = [i for i, x in enumerate(handsuits) if x == cloaksuit]
                suitmatches = [self.hand[index] for index in indices]
                cardscorelist = [[abs(handcard.value - spread[self.cloak].value), handcard] for handcard in suitmatches]
                scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
                scoreindex = scorelist.index(max(scorelist))
                # cloakselection = [value, card]
                cloakselection = cardscorelist[scoreindex]

            if centerselection and cloakselection:
                if centerselection[0] >= cloakselection[0]:
                    return centerselection[1], 'center'
                else:
                    return cloakselection[1], self.cloak

            elif centerselection:
                return centerselection[1], 'center'

            elif cloakselection:
                return cloakselection[1], self.cloak

            else:
                return maxvalcard, self.blind

        if self.game.phase == 'respaida':
            if 'center' in spread.keys():
                centersuit = spread['center'].suit
                center = 'center'
                if self.cloak in spread.keys():
                    cloaksuit = spread[self.cloak].suit
                    cloak = self.cloak
                if self.blind in spread.keys():
                    cloaksuit = spread[self.blind].suit
                    cloak = self.blind
            else:
                centersuit = spread[self.cloak].suit
                center = self.cloak
                cloaksuit = spread[self.blind].suit
                cloak = self.blind

            handsuits = [handcard.suit for handcard in self.hand]
            # copied this code directly from stackoverflow blame them if it doesn't work
            centerselection = None
            cloakselection = None
            if centersuit in handsuits:
                indices = [i for i, x in enumerate(handsuits) if x == centersuit]
                suitmatches = [self.hand[index] for index in indices]
                cardscorelist = [[abs(handcard.value - spread[center].value), handcard] for handcard in suitmatches]
                scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
                scoreindex = scorelist.index(max(scorelist))
                # centerselection = [value, card]
                centerselection = cardscorelist[scoreindex]

            if cloaksuit in handsuits:
                indices = [i for i, x in enumerate(handsuits) if x == cloaksuit]
                suitmatches = [self.hand[index] for index in indices]
                cardscorelist = [[abs(handcard.value - spread[cloak].value), handcard] for handcard in suitmatches]
                scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
                scoreindex = scorelist.index(max(scorelist))
                # cloakselection = [value, card]
                cloakselection = cardscorelist[scoreindex]


            if centerselection and cloakselection:
                if centerselection[0] >= cloakselection[0]:
                    return centerselection[1], center
                else:
                    return cloakselection[1], cloak

            elif centerselection:
                return centerselection[1], center

            elif cloakselection:
                return cloakselection[1], cloak

            else:
                cardscorelist = [[abs(handcard.value - spread[position].value), position, handcard] for position in spread.keys() for handcard in self.hand]
                scorelist = [cardscore[0] for cardscore in cardscorelist] # get scores
                scoreindex = scorelist.index(max(scorelist))
                totalmove = cardscorelist[scoreindex]
                return totalmove[2], totalmove[1]

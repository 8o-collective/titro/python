import random
from dataclasses import dataclass, field

from .card import Card
from .constants import Positions, Suit, Rank, MajorArcana

class Table:
	def __init__(self):
		# values must be initialized in this order
		self.deck = Deck(self)
		self.spread = Spread(self)
		self.discard = Discard(self)
	
	def __repr__(self):
		return f"\n\tDeck: {self.deck}\n\tSpread: {self.spread}\n\tDiscard: {self.discard}"

@dataclass
class Spread:
	table: Table = field(repr=False)
	cards: dict[Card] = field(default_factory=lambda: {position: None for position in Positions.Absolute})

	def draw(self):
		self.cards = { position: self.table.deck.draw() for position in Positions.Absolute }

	def discard(self):
		self.table.discard.add(self.cards.values())
		self.cards = { position: None for position in Positions.Absolute }

@dataclass
class Deck:
	table: Table = field(repr=False)
	cards: list[Card] = field(default_factory=list)

	def __post_init__(self):
		for suit in Suit:
			if suit == Suit.MAJORARCANA: continue # skip majorarcana

			for rank in Rank:
				self.cards.append(Card(rank, suit))

		for rank in MajorArcana:
			self.cards.append(Card(rank, Suit.MAJORARCANA))

	def draw(self):
		card = random.choice(self.cards)
		self.cards.remove(card)
		return card

@dataclass
class Discard:
	table: Table = field(repr=False)
	cards: list[Card] = field(default_factory=list)

	def add(self, cards):
		self.cards += list(cards)
		# if isinstance(cards, list):
		# 	self.cards += cards
		# else:
		# 	self.cards += [cards]